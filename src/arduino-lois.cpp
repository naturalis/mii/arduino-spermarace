// Created for: Naturalis Biodiversity Center
// Created by: Frank Vermeij & Titus Kretzschmar
// License: CC BY-SA 4.0

#include <Keyboard.h>

#define Inp_0 A0
#define Inp_1 A1
#define Inp_2 A2
#define Inp_3 A3
#define Inp_4 A4
#define Inp_5 A5
#define Inp_6 1
#define Inp_7 0

#define Led 13

int input_val = 0; // for calibration purposes, see loop 

void setup() {
  Serial.begin(115200);

  pinMode(Led,OUTPUT);
  digitalWrite(Led,LOW); // Led uit
}

void CheckInputs(void)
{
  if(analogRead(Inp_0) <= 20)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press('z');  // send a 'z' to the computer via Keyboard HID
    Serial.println("Input 1");  // Debug hulp
  } else {
    Keyboard.release('z');
  }

  if(analogRead(Inp_1) <= 20)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press('x');  // send a 'x' to the computer via Keyboard HID
    Serial.println("Input 2");  // Debug hulp
  } else {
    Keyboard.release('x');
  }
  if(analogRead(Inp_2) <= 20)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press('c');  // send a 'c' to the computer via Keyboard HID
    Serial.println("Input 3");  // Debug hulp
  } else {
    Keyboard.release('c');
  }
  if(analogRead(Inp_3) <= 20)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press('v');  // send a 'v' to the computer via Keyboard HID
    Serial.println("Input 4");  // Debug hulp
  } else {
    Keyboard.release('v');
  }
}

void loop() {
  CheckInputs();
  // input_val = analogRead(Inp_0); // for calibration purposes
  // Serial.println(input_val); // for calibration purposes
  delay(50);
}
